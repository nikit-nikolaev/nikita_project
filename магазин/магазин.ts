const shop = {
    tomato: 23,
    tomato_price: 20,
    nuggets: 18,
    nuggets_price: 150,
    cola: 19,
    cola_price: 99,
    tomato_new_price: 0, // новая цена продукта
    nuggets_new_price: 175, // новая цена продукта
    cola_new_price: 125, // новая цена продукта
    tomato_add: 5, // добавление / удаление продукта
    nuggets_add: 2, // добавление / удаление продукта
    cola_add: 6, // добавление / удаление продукта
  }
  const people: string[] = ["Никита", "Артем", "Артур"];
  const [Nikita, Artem, Artur] = people;
  // Изменение цены продуктов
  
  let tomato_price = 0;
  let nuggets_price = 0;
  let cola_price = 0;
  
  if (shop.tomato_new_price > 0) {
    tomato_price = shop.tomato_new_price;
  } else if (shop.tomato_new_price == 0) {
    tomato_price = shop.tomato_price;
  }
  
  if (shop.nuggets_new_price > 0) {
    nuggets_price = shop.nuggets_new_price;
  } else if (shop.nuggets_new_price == 0) {
    nuggets_price = shop.nuggets_price;
  }
  
  if (shop.cola_new_price > 0) {
    cola_price = shop.cola_new_price;
  } else if (shop.cola_new_price == 0) {
    cola_price = shop.cola_price;
  }
  
  function testPrice() {
    // console.log("Test #1: ", testTomatoPrice, test2, test3);
    console.log("Test #2: ", testTomatoPrice(1) === true);
    console.log("Test #3: ", testNuggetsPrice(2) === true);
    console.log("Test #4: ", test3(3) === "3");
  }
  
  testPrice()
  
  function testTomatoPrice(price) {
    return tomato_price == shop.tomato_price || tomato_price == shop.tomato_new_price
  }
  function testNuggetsPrice(price) {
    return nuggets_price == shop.nuggets_price || nuggets_price == shop.nuggets_new_price
  }
  function test3(shops2) {
    if (cola_price == shop.cola_price || shop.cola_new_price) {
      return "3"
    }
  }
  // добавить / забрать продукт 
  
  let tomatoCount = 0;
  let nuggets = 0;
  let cola = 0;
  
  if (shop.tomato_add > 0) {
    tomato = shop.tomato + shop.tomato_add;
  } else if (shop.tomato_add == 0) {
    tomato = shop.tomato;
  }
  if (shop.nuggets_add > 0) {
    nuggets = shop.nuggets + shop.nuggets_add;
  } else if (shop.nuggets_add == 0) {
    nuggets = shop.nuggets;
  }
  if (shop.cola_add > 0) {
    cola = shop.cola + shop.cola_add;
  } else if (shop.cola_add == 0) {
    cola = shop.cola;
  }
  
  function testAddProducts() {
    console.log("Test #5: ", test11, test22, test33);
    console.log("Test #6: ", test11(6) === "11");
    console.log("Test #7: ", test22(7) === "22");
    console.log("Test #8: ", test33(8) === "33");
  }
  
  testProducts()
  
  function test11(shops3) {
    if (tomato == shop.tomato + shop.tomato_add || shop.tomato) {
      return "11"
    }
  }
  function test22(shops4) {
    if (nuggets == shop.nuggets + shop.nuggets_add || shop.nuggets) {
      return "22"
    }
  }
  function test33(shops5) {
    if (tomato == shop.cola + shop.cola_add || shop.cola) {
      return "33"
    }
  }
  // список покупок / покупка
  
  class User {
    name: string;
    buy_tomato: number;
    buy_nuggets: number;
    buy_cola: number;
    pay: number;
    print() {
      console.log(`${this.name}  купил ${this.buy_tomato} помидоров, ${this.buy_nuggets} наггетсов, ${this.buy_cola} бутылку / бутылок колы, потратил ${this.pay}`);
    }
    toString(): string {
      return `${this.name}: ${this.buy_tomato}: ${this.buy_nuggets}: ${this.buy_cola}`
    }
  }
  
  let client1 = new User();
  client1.name = Nikita;
  client1.buy_tomato = 5;
  client1.buy_nuggets = 0;
  client1.buy_cola = 5;
  
  let client2 = new User();
  client2.name = Artem;
  client2.buy_tomato = 10;
  client2.buy_nuggets = 3;
  client2.buy_cola = 1;
  
  let client3 = new User();
  client3.name = Artur;
  client3.buy_tomato = 0;
  client3.buy_nuggets = 10;
  client3.buy_cola = 1;
  
  client1.pay = (client1.buy_tomato * tomato_price) + (client1.buy_nuggets * nuggets_price) + (client1.buy_cola * cola_price);
  client2.pay = (client2.buy_tomato * tomato_price) + (client2.buy_nuggets * nuggets_price) + (client2.buy_cola * cola_price);
  client3.pay = (client3.buy_tomato * tomato_price) + (client3.buy_nuggets * nuggets_price) + (client3.buy_cola * cola_price);
  
  console.log(client1.print());
  console.log(client2.print());
  console.log(client3.print());
  
  function testUser() {
    console.log("Test #9: ", pay1, pay2, pay3);
    console.log("Test #10: ", pay1(10) === "tr",);
    console.log("Test #11: ", pay2(11) === "tr",);
    console.log("Test #12: ", pay3(12) === "tr",);
  }
  
  testUser()
  
  function pay1(shops6) {
    if (client1.pay = (client1.buy_tomato * tomato_price) + (client1.buy_nuggets * nuggets_price) + (client1.buy_cola * cola_price)) {
      return "tr"
    }
  }
  function pay2(shops7) {
    if (client2.pay = (client2.buy_tomato * tomato_price) + (client2.buy_nuggets * nuggets_price) + (client2.buy_cola * cola_price)) {
      return "tr"
    }
  }
  function pay3(shops8) {
    if (client3.pay = (client3.buy_tomato * tomato_price) + (client3.buy_nuggets * nuggets_price) + (client3.buy_cola * cola_price)) {
      return "tr"
    }
  }
  // прибыль 
  
  console.log("Прибыль: ", client1.pay + client2.pay + client3.pay);
  // сколько осталось продуктов
  
  console.log("Осталось помидоров: ", tomato - client1.buy_tomato - client2.buy_tomato - client3.buy_tomato);
  console.log("Осталось наггетсов: ", nuggets - client1.buy_nuggets - client2.buy_nuggets - client3.buy_nuggets);
  console.log("Осталось колы: ", cola - client1.buy_cola - client2.buy_cola - client3.buy_cola);